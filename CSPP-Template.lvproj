﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="21008000">
	<Property Name="CCSymbols" Type="Str">CSPP_BuildContent,CSPP_Core;CSPP_WebpubLaunchBrowser,None;MQTT_DSC,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project is used to develop this application based on NI ActorFramework and CS++ libraries.

SHORT_DESCRIPTION

Please refer also to README.md.

Author: name@domain

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{007BCD2C-B791-4FCA-BE7B-FD9C3EFAFFE7}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_LaunchedDT</Property>
	<Property Name="varPersistentID:{01331C05-4CA3-4868-BA3D-F22688F40064}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_PollingIterations</Property>
	<Property Name="varPersistentID:{032A7DF0-73F9-4206-B9FA-9E714FF34180}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingTime</Property>
	<Property Name="varPersistentID:{036ADF4B-515D-466D-8821-C9CE847894DF}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{036C235B-C385-49A6-8035-69D4C3CA936F}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{04AEA2A6-AD77-448A-896A-F4FE87407003}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{06B93FAF-494E-46B4-993B-03CA9DEAC6A1}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterfaceProxy_Activate</Property>
	<Property Name="varPersistentID:{08149E89-8796-454C-922E-5C6C27E5ED74}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_SystemID</Property>
	<Property Name="varPersistentID:{081B4472-4238-4E60-B3DE-1BAC8BE8B71A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-RemoteController</Property>
	<Property Name="varPersistentID:{082A4B43-D4E9-4ED1-9405-749074E1251F}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_LaunchedT</Property>
	<Property Name="varPersistentID:{08CD3C86-3599-4B9C-A9BE-7E72918C9594}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/String</Property>
	<Property Name="varPersistentID:{092F5467-60FC-4B52-AE27-AAA95AEE6B86}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingInterval</Property>
	<Property Name="varPersistentID:{09328649-CEF4-4505-A399-CBF7A6A282AD}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_ClassName</Property>
	<Property Name="varPersistentID:{09BDEE67-4E17-4F37-A439-2A5B615EEED0}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{0A8D2794-025C-4616-9C0E-D17C2DAE0FCE}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Set-RemoteController</Property>
	<Property Name="varPersistentID:{0A968C8E-F525-4B4C-A837-C1376774631D}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{0AF5C705-675E-45DE-A363-983A7FE38445}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{0D3873B8-F867-46B8-8543-88B9EC57CAF0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{0E422A01-F574-44BA-8E4D-637E670212D8}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/StartActor_MsgClass</Property>
	<Property Name="varPersistentID:{0F08FBA0-0233-41AA-B748-A5282209BA6E}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_PollingInterval</Property>
	<Property Name="varPersistentID:{1027A2F6-8A6F-44F2-9EF3-A7E45E3CE74F}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{10389977-BE0A-4F5A-BBFE-2504AA4CC650}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_Set-RemoteController</Property>
	<Property Name="varPersistentID:{11BFDD8F-A732-40F2-A0DD-E66B25BF47DA}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{12B542A5-ECA7-4D3A-B20D-0DCB9FA56701}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{12D511DA-759B-4904-A56A-13467DC054E7}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_LaunchedDT</Property>
	<Property Name="varPersistentID:{1321EE49-FFBA-4630-A526-F21BC49F2F60}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{138E934B-B60E-40C4-8081-DD0EDC39619A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_MsgClass</Property>
	<Property Name="varPersistentID:{14E8D8AC-E236-4804-B58B-48F77178032A}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{15409A2B-E0B9-4761-97FC-29B30925CC34}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{16A13434-8914-4AC9-940D-0A25F0503F0D}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{1843393D-ADCA-4502-B670-B421C5D661A4}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Error</Property>
	<Property Name="varPersistentID:{1882F0C5-88E0-4B1B-860B-3520AA91F515}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_PollingTime</Property>
	<Property Name="varPersistentID:{18B015CB-9A83-40DC-9B62-FD59FFEBB7BA}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{18C083EE-7328-460F-801C-EE8E18474F49}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_LaunchedT</Property>
	<Property Name="varPersistentID:{19026EF7-E2C9-4437-8C5D-F5B848003C97}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_LaunchedDT</Property>
	<Property Name="varPersistentID:{1985BA19-895F-4EC7-B5C5-9713C38A7042}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_MsgCounter</Property>
	<Property Name="varPersistentID:{1A3C81BC-3BA0-4933-88E5-8EA4FB70D742}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorMessage</Property>
	<Property Name="varPersistentID:{1A8991FF-3A11-4B87-BC85-F83AFAAB3D3A}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{1D7B99F7-D05E-4A67-A3FF-A63B247CF623}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_LaunchedT</Property>
	<Property Name="varPersistentID:{1E2AB56D-5F1F-43E3-A352-B4F7608B6D0F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Error</Property>
	<Property Name="varPersistentID:{22D6E109-FF28-46D1-915E-63261B1C94B9}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{230C14F9-14CE-4693-ACAB-50201A2EFA14}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{23BD2AF8-A07E-40C4-B6FA-F73D4019BEB3}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_LaunchedDT</Property>
	<Property Name="varPersistentID:{245AAF22-76F8-405E-8A99-D009A46200BE}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{24C164C6-1E8A-442A-926B-DF8B1E99BE20}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{253B82BB-5F86-4F77-8C50-A838BAD94BBC}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorStatus</Property>
	<Property Name="varPersistentID:{25B8C2B7-E23F-4C96-AA1D-74FE26220925}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{26CBDCE1-B239-4046-9FB1-373DC2B4BB08}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{2704DCA7-C7C1-4895-B98B-BD234BAA70A8}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_PollingMode</Property>
	<Property Name="varPersistentID:{27A29470-8DB6-4265-A3A5-B95D81F93D13}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-RemoteController</Property>
	<Property Name="varPersistentID:{27EC4A4D-215E-463A-B93A-EAC2AC37F36A}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/StartActor_SystemID</Property>
	<Property Name="varPersistentID:{285A7742-F9EF-46EB-A6D0-1F3620110EAA}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Test-Variable</Property>
	<Property Name="varPersistentID:{28CF1838-4D90-42FA-8CB9-5FF39D02A0FA}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{2974F87C-5D3A-4C42-8B8F-7A62048C68FA}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingDeltaT</Property>
	<Property Name="varPersistentID:{2A0981F0-D166-4E60-A0B8-07603075FED7}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_PollingCounter</Property>
	<Property Name="varPersistentID:{2BC4614B-DF29-4EDD-8ADE-2C488BA385CF}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/StartActor_LaunchedDT</Property>
	<Property Name="varPersistentID:{2C24A53E-F719-47CF-92E3-4C1A34ED8FA1}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_MsgCounter</Property>
	<Property Name="varPersistentID:{316550E8-815E-430E-8F9B-E054E0395ACD}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Initialized</Property>
	<Property Name="varPersistentID:{31A4A58F-F5C6-40F7-8F4B-C362C215FA4F}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{3272D0B6-9ADD-4909-919B-9D5160FE209C}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{353432AD-571D-4FC5-9C1A-BF4373B9C6DA}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{35C9F498-86AA-44F9-B438-7FD9E5C3CBB1}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{35EEE3CF-39C1-464F-AB8E-E98313A8229B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/BeepProxy_Activate</Property>
	<Property Name="varPersistentID:{36E5897D-A098-423A-9CF1-0CD28E4B6F2F}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{370BDB6A-6FC9-43E2-BE1A-6B9857E4A40F}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{37467BF2-E30D-47CA-90AD-BCA0427EC698}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingTime</Property>
	<Property Name="varPersistentID:{395F25D1-D762-4DDF-BCA7-4613F1CB8527}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_PollingMode</Property>
	<Property Name="varPersistentID:{39A88F56-7711-400C-B094-D1B0317198CC}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_RemoteController</Property>
	<Property Name="varPersistentID:{39F0963B-1C0B-43CD-B0E0-CE57C87E9452}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActorProxy_Activate</Property>
	<Property Name="varPersistentID:{39FF2268-E6FC-40CF-9E26-2D09630D99D5}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_LaunchedDT</Property>
	<Property Name="varPersistentID:{3AD1B768-4E13-4366-8925-FA2389F7E9EB}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_PollingTime</Property>
	<Property Name="varPersistentID:{3ADC96CA-E600-451A-A547-A3993BDE7C2F}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_LaunchedT</Property>
	<Property Name="varPersistentID:{3BB82F08-B4B7-4D2D-ABE9-9A68ED184A0F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/StartActor_LaunchedDT</Property>
	<Property Name="varPersistentID:{3BE2EAC3-305E-40A6-AB32-4594E4F60463}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{3BE4CF23-9AA3-41CA-9657-9D9C5F1785CD}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterfaceProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{3D631F93-F4C3-43BE-B386-6A5D6281AB6A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_LaunchedT</Property>
	<Property Name="varPersistentID:{3E175458-9804-4634-A771-4969902D2ED1}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{3ED874E5-A050-43C1-A493-34FAD5DA9176}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{413C7655-E419-41AF-816F-2018D69003BD}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{439A8451-8ACE-4D8D-825F-1CBF49B32C37}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-TimeStamp</Property>
	<Property Name="varPersistentID:{44328294-3E22-4716-BB82-1BC2348C3E18}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{45358A97-16F7-449F-B981-0E6552AC9D54}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/IntegerArray</Property>
	<Property Name="varPersistentID:{46803F68-1B57-4278-A87B-DA78E651CBF0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_LaunchedDT</Property>
	<Property Name="varPersistentID:{476AB865-C5C9-413A-B92C-AD3180570669}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{47943CBE-7BC8-4F95-850A-1BD0FEE94AF4}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingIterations</Property>
	<Property Name="varPersistentID:{48E8B604-163A-4C97-8032-1BBE1AA72EAB}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/StartActor_MsgClass</Property>
	<Property Name="varPersistentID:{499E1124-3F2B-4CFD-B2E3-36D04A829D6D}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/StartActor_LaunchedT</Property>
	<Property Name="varPersistentID:{4B165E3A-BBF0-4DA4-B588-C6A3B94459E5}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{4B44F377-B55F-41FF-83D8-32F4E8E28C72}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{4B85C471-10BE-4860-97E5-537826736BC3}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Boolean</Property>
	<Property Name="varPersistentID:{4B9C5A1F-F111-45EB-8B11-6578AD69C5DD}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-String</Property>
	<Property Name="varPersistentID:{4C4EFDB2-D677-46FF-BA25-473C5FBEB039}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingInterval</Property>
	<Property Name="varPersistentID:{4D0F36D9-5697-4CAD-AF0B-54034F235810}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/StartActor_MsgCounter</Property>
	<Property Name="varPersistentID:{4D18E9A7-2418-4043-A6D0-CA2CD0638732}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{4DD08A16-4696-40B3-81FB-6AC5E3A478EC}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_SystemID</Property>
	<Property Name="varPersistentID:{4F5436D2-436E-4C06-A06D-4DDD80541BEA}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_LaunchedT</Property>
	<Property Name="varPersistentID:{5164E24B-AB6F-4487-BB97-6F4074AE7EA5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{51A597EC-59B1-4709-8577-01C40206D92C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_SystemID</Property>
	<Property Name="varPersistentID:{51E3C320-21A0-46CF-95B6-D3F60DDE5F14}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Error</Property>
	<Property Name="varPersistentID:{5202A381-3233-4506-A23F-40328D7C3372}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{52CA4CA2-96E3-4EBE-8B65-77E2C2EA884C}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{53AE040A-758D-490F-8598-6BC391873622}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingMode</Property>
	<Property Name="varPersistentID:{53FB95E3-4DAE-4A02-8ABB-32A6A1451E3E}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{54815DA9-BD54-4982-9ED0-417CDF81669D}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_Initialized</Property>
	<Property Name="varPersistentID:{5521F535-44E7-4D6D-9EF9-395DC990211C}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/TimeStamp</Property>
	<Property Name="varPersistentID:{57B7C91B-54C5-4A29-87C8-1DA20B3FF035}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{59BA85D3-90F6-43C1-B981-2DEC6B5E65D5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{5B8DB5F0-F5EA-4BF8-868B-BA9EFDBC0DB6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-Selftest</Property>
	<Property Name="varPersistentID:{5BAF1320-C5E5-4FC7-9713-590A872064BA}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/WatchdogProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5CB703D2-5EC2-42B3-82DB-EA0758A31ECC}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{5CC01F30-36D8-453E-8006-946B24B171E1}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_RemoteController</Property>
	<Property Name="varPersistentID:{5D4D753C-2786-418A-BEE0-5C2B360B4BCD}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/BeepProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5D523BCB-5DC1-4C19-9432-3FA76B655FBB}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_LaunchedT</Property>
	<Property Name="varPersistentID:{5DFB4B31-9181-4949-93D9-89BB875B5787}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{5FEC2A33-E3F7-49FA-8EB8-273BEAE31DF4}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{6019EBB1-19A4-42CF-BAA2-AD3441FDAFD1}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/StartActor_ClassName</Property>
	<Property Name="varPersistentID:{63F2B040-0723-467D-95D8-5DDC45789FFC}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_LaunchedT</Property>
	<Property Name="varPersistentID:{64BD17B0-04B2-4036-945A-2C1ACF91F5BE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_MsgClass</Property>
	<Property Name="varPersistentID:{660E98BC-356F-423A-BEC9-2FA5A43399E3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorStatus</Property>
	<Property Name="varPersistentID:{663DF93F-21CA-4220-831C-62826F91DB5F}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_ErrorMessage</Property>
	<Property Name="varPersistentID:{671D46BF-8ABA-4DE0-BE8D-C41FA61692C3}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{6C39A500-C9F6-4813-8916-D67F4586CEED}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{6C4D34E4-D073-4202-A44D-4E3FF56F0BF6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{6D31C13D-AC26-48D4-9534-8E051E13A32E}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{6D43DBF5-A80F-4728-A73F-2A5193B083B2}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{6D5053F7-B277-4C37-AA22-7251DEABB4F9}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorCode</Property>
	<Property Name="varPersistentID:{6DFB121F-1BED-45F9-963D-4AD1C9085907}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/BeepProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{6E5DD228-F78A-462A-B355-F85032BD1B52}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_Initialized</Property>
	<Property Name="varPersistentID:{6F6C5A8C-8848-460E-8E33-4AE69507082C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_LaunchedDT</Property>
	<Property Name="varPersistentID:{6FFED4C8-653F-496B-8B3A-F43CF064C095}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Initialized</Property>
	<Property Name="varPersistentID:{70D0C586-E6DE-45AA-A11A-181E1C1B9F83}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_MsgClass</Property>
	<Property Name="varPersistentID:{70F0FD87-1F31-46C6-AD18-38B1A2A72E4B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingIterations</Property>
	<Property Name="varPersistentID:{71ACF4A6-0A22-42AF-BE36-01691F08CD8D}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Waveform</Property>
	<Property Name="varPersistentID:{74D2EB5E-EF74-46BA-BDBF-B3489FE99778}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_SystemID</Property>
	<Property Name="varPersistentID:{767A37EF-B9B2-4224-8FEB-F9ACB6DB082E}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-StringArray</Property>
	<Property Name="varPersistentID:{76E54711-248D-46DA-8440-19A68DCFA9CE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{773D6543-5404-44AF-9CDF-1412D2C87C0D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{784F9E83-1F33-4D40-97FC-F8CA27FB9505}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{78864DEF-7AE0-467A-A5C4-70CF0DD24365}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-Reset</Property>
	<Property Name="varPersistentID:{78B72029-80B4-403E-8F41-7A79EA0B6F1E}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{79A4D5CD-0F85-46DD-B09E-8FA2871DBF14}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_ClassName</Property>
	<Property Name="varPersistentID:{7A3AC543-D65A-4866-B12C-587448D45961}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_SystemID</Property>
	<Property Name="varPersistentID:{7A77C989-9BA8-490A-BEC8-14CD5B63D8D3}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Integer</Property>
	<Property Name="varPersistentID:{7BE9F631-39AB-4145-AD4A-2790840F803C}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/BooleanArray</Property>
	<Property Name="varPersistentID:{7E5F2E47-BBD1-4068-81B4-846C5F431461}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_ClassName</Property>
	<Property Name="varPersistentID:{7F271FB0-57BD-455A-AE12-47C06BD575CD}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/DoubleArray</Property>
	<Property Name="varPersistentID:{7F62AEB8-E25E-4906-AC7D-9CBFE16AB759}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ClassName</Property>
	<Property Name="varPersistentID:{7FE1E720-3EDF-4AF1-B864-0250C51F9F0E}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{805C50EF-0665-448B-9D5D-5DD7263B06C9}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{816ADF4F-95CC-4FB8-8402-794E6C34618A}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{8207A06F-68CC-4FDD-B6A3-814BD0802287}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{8265EFE7-B259-436E-85CB-CCE3B871024A}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{85120A44-5D68-46F1-8525-830968AF9DE9}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_PollingDeltaT</Property>
	<Property Name="varPersistentID:{851431B7-05CC-4C75-A63B-E1DC1F09B1CF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{86A5CEB2-02EB-413F-935C-64F5A4E24007}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_MsgClass</Property>
	<Property Name="varPersistentID:{89D37780-6BC5-4214-8682-BF30C974ADB8}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_ErrorStatus</Property>
	<Property Name="varPersistentID:{8A3FE12A-0FA5-4567-B863-A8D36488E945}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{8C022847-C25C-49E7-9A87-47C790B0373C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActorProxy_Activate</Property>
	<Property Name="varPersistentID:{8E02D483-D055-421E-B9BC-57D739D6DE32}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{901D4C06-649F-40F7-AD1F-25CFDBDCA03D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{92205C0B-C5AA-43BC-ACB3-7D8EF4564048}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Initialized</Property>
	<Property Name="varPersistentID:{931CB3D1-F4D7-4B00-A532-897E9146A057}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorCode</Property>
	<Property Name="varPersistentID:{93877FA4-9268-431C-B0DE-E85C73221989}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{93DCBC34-CCB0-4AC4-9148-CD4AF4F471B3}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{94E78404-8921-4EE8-956F-FC280900CE53}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_MsgCounter</Property>
	<Property Name="varPersistentID:{971E1159-0DA7-4DDF-8082-FE6A89D6F6D0}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_PollingCounter</Property>
	<Property Name="varPersistentID:{981F095A-A749-46F3-9C91-E3B212B9F6D0}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/StartActor_LaunchedT</Property>
	<Property Name="varPersistentID:{9849132E-3BAA-4903-BFC5-CA0C54A46DC3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Error</Property>
	<Property Name="varPersistentID:{9A310C64-4F99-479B-B618-50829D7E8781}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-Double</Property>
	<Property Name="varPersistentID:{9C3FD2D0-35C7-4391-9E75-5750646C32E9}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_MsgCounter</Property>
	<Property Name="varPersistentID:{9D3F8C9E-4A4C-4D0E-BD73-9220474E38B1}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{9D683274-B708-4EE8-95EE-73C75AD46EB0}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_RemoteController</Property>
	<Property Name="varPersistentID:{9E1A28FC-780A-4BAA-B4AC-54933B6DB2EC}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_MsgCounter</Property>
	<Property Name="varPersistentID:{9E8FA303-E324-4586-95E5-FBECF0217BFE}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_MsgCounter</Property>
	<Property Name="varPersistentID:{A01AA041-7E1A-47C4-B647-7EC0CC720B93}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{A0475854-9C35-4AF5-B221-32CE4C5E6088}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_ErrorCode</Property>
	<Property Name="varPersistentID:{A056D4EB-A7DB-4BB6-A5B3-B63804F761DB}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{A088265E-7239-4C2D-9D95-4FEED10EF748}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{A224EF01-AE8A-4F23-BAFC-A64CDE72497B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-RemoteController</Property>
	<Property Name="varPersistentID:{A304680C-9E0C-4F0A-9B2F-9DEB0E8655CE}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{A3E30524-394C-43B8-9BCF-29BBD2A3AA67}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{A44347E7-F0EF-4BA0-9F0A-A6B5BB3283DC}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{A4654267-0785-47DF-8DA9-9A051A182E3F}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_ClassName</Property>
	<Property Name="varPersistentID:{A48728D2-1C93-44A6-A477-E501E05EF842}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A57992F1-1BDC-44FE-82B6-24110AAE08A5}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/StringArray</Property>
	<Property Name="varPersistentID:{A73756A0-A51E-4398-9BD8-268591E0C4B1}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{A73CBEB9-8111-4535-A021-627DB7134EF5}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-Error</Property>
	<Property Name="varPersistentID:{A743541E-8C65-4D33-88F1-2457B8A9595D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{A7905AAB-4155-4B95-8CE5-CE06B56A61EA}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{A7D9FA97-DD5F-481B-ADDB-4808797A4EC8}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{A7F3DF6A-9386-4790-87CB-3639B968DD05}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_LaunchedDT</Property>
	<Property Name="varPersistentID:{A9713CBF-0D51-45FD-B152-63BAE649359D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{A998EAC3-45C0-4DAB-BB8F-4255BF26DD82}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/StartActor_SystemID</Property>
	<Property Name="varPersistentID:{A9C126D2-DD22-4383-979A-9362677DE42C}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_RemoteController</Property>
	<Property Name="varPersistentID:{AB149E42-29CA-4B7C-ABC9-FC9D5A13A2B3}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_ErrorCode</Property>
	<Property Name="varPersistentID:{AB4ADC75-110F-4177-BAD8-2F237B6A83DF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_LaunchedT</Property>
	<Property Name="varPersistentID:{AD058440-A0D0-4045-9188-53C5F8E06665}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{AD30D7F8-A694-4BC8-B125-E0A75692FAAB}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_RemoteController</Property>
	<Property Name="varPersistentID:{AE5E8306-DAB5-4919-8C5A-207E35856DD0}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{AE920AB1-E6C7-4DFF-86AF-25656A1DFDAF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{B02A58A5-7F62-432B-9F31-CDA9EBCEE7E7}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_PollingInterval</Property>
	<Property Name="varPersistentID:{B19B25D0-A8CD-43A3-AC33-61060E169FE6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_MsgClass</Property>
	<Property Name="varPersistentID:{B1DC02D6-EE9C-4B06-9985-F2D592D29D49}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Error</Property>
	<Property Name="varPersistentID:{B63DAEF4-DD0C-44C8-832E-17AE3BF28E9F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{B793682F-16F3-4387-95AF-AAD62449A1A2}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-DoubleArray</Property>
	<Property Name="varPersistentID:{B90DA07F-9A95-4183-9E45-C530997520FC}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{B924656A-58EB-4A81-9959-0DAEE4E52800}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_MsgCounter</Property>
	<Property Name="varPersistentID:{BA8C7ABC-7EAC-4690-BC0A-900F38D8DC59}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Test-Variable_WDAlarm</Property>
	<Property Name="varPersistentID:{BAAB10B0-4B5C-40D6-BC88-35C1180EAE14}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-Integer</Property>
	<Property Name="varPersistentID:{BB284466-C64C-47EB-A289-DA4965F2C9F2}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-Waveform</Property>
	<Property Name="varPersistentID:{BBA98AA2-7560-4D72-91DF-A1EF423246DA}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{BBC3E53B-5032-44D6-90C2-C1B18A337795}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{BBE7CB13-DC1E-4D27-97DF-F6F55FB2D769}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{BBF3CD4C-EC2F-4F65-9951-BE1A542187C4}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingMode</Property>
	<Property Name="varPersistentID:{BD3A2C5F-4539-49F6-B5A4-F1E38032A90C}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/WaveformArray</Property>
	<Property Name="varPersistentID:{BD545146-15BF-43BB-B951-AB3E96C61F77}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_SystemID</Property>
	<Property Name="varPersistentID:{BDDE4FEA-D613-4415-8936-BE157A0123DB}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingCounter</Property>
	<Property Name="varPersistentID:{BDE4AC9A-16BC-4CB9-87EA-980CCC0A27C8}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_PollingIterations</Property>
	<Property Name="varPersistentID:{BDF924BE-A3B4-41BE-A9EB-67C01A555D3B}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{BDFF2152-3DC0-4F19-AE9A-4973F4BB8BA0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{C12A2F56-595C-408C-97FF-9E1B754511D3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ClassName</Property>
	<Property Name="varPersistentID:{C2021F58-876E-49D8-931C-C3E56EFEABBE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{C239DCA6-F9EA-4B0E-90C4-C02B3820071B}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{C43C7D58-718E-4D84-8102-C9ADA1236FA7}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-BooleanArray</Property>
	<Property Name="varPersistentID:{C4D6CB2E-F1E8-4F64-912A-A03809D0778E}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_Initialized</Property>
	<Property Name="varPersistentID:{C60F8165-B79A-4A9C-B40D-BC7D9F5597AF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{C71F9E6D-6375-41D6-886B-A4A1213C318D}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_MsgClass</Property>
	<Property Name="varPersistentID:{C80423CD-CB13-4E88-8580-274252136563}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{C816621C-7F68-4FA2-86E9-42625FFF7A4C}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Initialized</Property>
	<Property Name="varPersistentID:{CA092E48-9636-4E73-9F00-A6D8BF3A20F6}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ClassName</Property>
	<Property Name="varPersistentID:{CB947676-A4FC-41DE-8BE9-1F41D9B34B0B}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{CCD5AE79-C24C-4201-BAE4-E9A3FE77A02A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{CE09465C-36CF-4FBB-8295-28CAC311D5A6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_SystemID</Property>
	<Property Name="varPersistentID:{D0883624-FCC1-4BCD-BF12-91A525948EF6}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/BeepProxy_Activate</Property>
	<Property Name="varPersistentID:{D224F917-06D2-40A5-BDFB-FA6715701AF3}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_Set-RemoteController</Property>
	<Property Name="varPersistentID:{D3772573-6B4F-4F66-8B19-B5A8CA087196}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingCounter</Property>
	<Property Name="varPersistentID:{D3FF171D-BEE0-4C9D-9099-8A5EA64D7ADB}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-RemoteController</Property>
	<Property Name="varPersistentID:{D48DE465-C791-4C84-BB58-658D501340B8}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_Error</Property>
	<Property Name="varPersistentID:{D4D7C24E-D7E1-4A59-82E9-46F57FFE6A8E}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_SystemID</Property>
	<Property Name="varPersistentID:{D8148F3B-A844-44E2-86A4-64F24155478C}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_Initialized</Property>
	<Property Name="varPersistentID:{D9328AFC-433E-4439-B186-192C86BA893B}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_ErrorMessage</Property>
	<Property Name="varPersistentID:{D93E586F-3C46-437F-8D7D-B16E519CE8EB}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_MsgClass</Property>
	<Property Name="varPersistentID:{D971C09B-5BB5-429E-9007-104E76ED1508}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{DA4B5F02-BF46-4C95-A415-4268042B9D15}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{DAD07B25-AFAD-4038-9E56-F26191BD8995}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Double</Property>
	<Property Name="varPersistentID:{DB8F4682-39FD-4CD0-A3B3-45B595BDFB2A}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{DC767400-2440-4630-9758-25CB214BF055}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-WaveformArray</Property>
	<Property Name="varPersistentID:{DC92F411-134E-49F0-9CB6-D1D6DF85BDC6}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_PollingDeltaT</Property>
	<Property Name="varPersistentID:{DD207A19-5278-4E21-A65A-DD0CB2FBB7FD}" Type="Ref">/My Computer/Packages/MQTT/MQTTSVInterface.lvlib/MQTTSVInterface_ErrorStatus</Property>
	<Property Name="varPersistentID:{DDB87262-1C68-4B85-8B35-5142A63EBC06}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_MsgClass</Property>
	<Property Name="varPersistentID:{DF39D883-1810-450C-8D09-71B839811441}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SystemID</Property>
	<Property Name="varPersistentID:{E0B33B20-3ADF-4A62-ADF2-C375CDF8CB2F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{E0C74B09-8FB6-4193-8FCB-0F9B8B5A97DE}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{E110560C-A99C-4E81-82E3-234C263552AB}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{E1BD6337-7335-4314-8555-2352F8220F21}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/WatchdogProxy_Activate</Property>
	<Property Name="varPersistentID:{E27F1048-F66F-41EF-9B3E-901724E0A595}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{E2807C7A-D74B-41AD-A4AD-C63DF9C758D8}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_MsgClass</Property>
	<Property Name="varPersistentID:{E46D40DB-B19B-401B-B3C2-26C832AB2872}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_RemoteController</Property>
	<Property Name="varPersistentID:{E4DFEE42-9565-4BCC-A94E-3E494A53134C}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/ObjectManager_MsgCounter</Property>
	<Property Name="varPersistentID:{E55EDF91-5622-4701-80E1-DA4EA0101251}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorMessage</Property>
	<Property Name="varPersistentID:{E643E644-5011-4F7F-844C-D6ED95ACD973}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ClassName</Property>
	<Property Name="varPersistentID:{E7DF3CA3-5817-44E5-9833-B898766A5AD2}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{E812778E-7D4D-46B5-9A48-B8886AA28F0F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{EB04DA38-FFBC-432B-934F-93EF70F07152}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/StartActor_MsgCounter</Property>
	<Property Name="varPersistentID:{EBC5DE33-31B9-4899-902C-71527FD13E4B}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-IntegerArray</Property>
	<Property Name="varPersistentID:{ECF45F17-C1D0-42C9-8F79-8941A4BA0403}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{ED121830-EBCE-4322-ADD7-1EA1506BEDD6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingDeltaT</Property>
	<Property Name="varPersistentID:{EDC36392-FED5-4D0C-8DF9-3AFFED434D85}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{EE8ADF55-1C0A-4890-9DE1-2E19ED56B03C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_RemoteControllerPrevious</Property>
	<Property Name="varPersistentID:{F1295E39-9F63-4E0A-A9CA-AA06F51AABC7}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_MsgCounter</Property>
	<Property Name="varPersistentID:{F18E596E-21D8-4ABD-817E-771A41359916}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ClassName</Property>
	<Property Name="varPersistentID:{F4AA2D88-A0EC-45E6-ABE0-8719E5A557D2}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{F51987AD-81FD-480A-8C1C-DCEEDE55B09D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Initialized</Property>
	<Property Name="varPersistentID:{F51D3B76-58E6-4515-AFB3-5CC07E520724}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{F60841D5-A748-4F70-A65B-EE98F20B89C2}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{F6B80B5D-C71D-4FC6-A5E4-1E2FC3938ECF}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{F8612543-106E-4402-BDEA-88548117A171}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/StartActor_ClassName</Property>
	<Property Name="varPersistentID:{F8B7F5DC-9EC9-4A5E-8F8B-F5EE6FA1CC5C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{F97C0D2B-9E1B-4180-A90B-C81B0F940D00}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_LaunchedDT</Property>
	<Property Name="varPersistentID:{FA06F7CF-5A7B-4C0E-AEEB-0E9E4D43241A}" Type="Ref">/My Computer/Packages/MQTT/MQTTSV.lvlib/Set-Boolean</Property>
	<Property Name="varPersistentID:{FB6AD917-BDE5-43D5-BDC5-8D046D98DF3B}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{FC453073-D0AA-411C-99F7-C37CFA236425}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{FCF72D76-A50E-4483-A590-83B1225257F9}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{FD440257-3596-42E6-916B-9DA82453E1E4}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_RemoteController</Property>
	<Property Name="varPersistentID:{FD9AADAA-A863-4884-B744-BC885D5C603A}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorCode</Property>
	<Property Name="varPersistentID:{FF49BE04-E0DB-45D5-A6DA-7E24EBADC0FF}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{FFB6C830-B632-4A33-A2EB-20DBFB110F6A}" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib/Beep_Error</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
			<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
		</Item>
		<Item Name="Documentation" Type="Folder"/>
		<Item Name="EUPL License" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
		</Item>
		<Item Name="instr.lib" Type="Folder"/>
		<Item Name="Packages" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
					<Item Name="CSPP_QMsgLogMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_QMsgLogMonitor/CSPP_QMsgLogMonitor.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
					<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_Core/Change_Log.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_Core/Release_Notes.txt"/>
					<Item Name="VI-Analyzer-Configuration.cfg" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Configuration.cfg"/>
					<Item Name="VI-Analyzer-Results.rsl" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Results.rsl"/>
					<Item Name="VI-Analyzer-Spelling-Exceptions.txt" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Spelling-Exceptions.txt"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_NAInitialized Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_NAInitialized Msg/CSPP_NAInitialized Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
					<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent-Linux.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent-Linux.vi"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="CSPP_DeploymentExample.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_DeploymentExample.vi"/>
				<Item Name="CSPP_Post-Build Action.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_Post-Build Action.vi"/>
			</Item>
			<Item Name="MQTT" Type="Folder">
				<Item Name="CSPP_MQTT.ini" Type="Document" URL="../Packages/CSPP_MQTT/CSPP_MQTT.ini"/>
				<Item Name="CSPP_MQTTBroker.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/Classes/CSPP_MQTTBroker.lvlib"/>
				<Item Name="CSPP_MQTTConnection.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/Classes/CSPP_MQTTConnection.lvlib"/>
				<Item Name="CSPP_MQTTContent.vi" Type="VI" URL="../Packages/CSPP_MQTT/CSPP_MQTTContent.vi"/>
				<Item Name="CSPP_MQTTMonitor.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/Actors/CSPP_MQTTMonitor.lvlib"/>
				<Item Name="CSPP_MQTTMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/Classes/CSPP_MQTTMsgLogger.lvlib"/>
				<Item Name="CSPP_MQTTUtilities.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/Utilities/CSPP_MQTTUtilities.lvlib"/>
				<Item Name="MQTTSV.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/MQTTSV.lvlib"/>
				<Item Name="MQTTSVInterface.lvlib" Type="Library" URL="../Packages/CSPP_MQTT/MQTTSVInterface.lvlib"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
				<Item Name="CSPP_ObjectManager_SV.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_SV.lvlib"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
				<Item Name="CSPP_SystemMonitor_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/CSPP_SystemMonitor_SV.lvlib"/>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_Utilities_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/CSPP_Utilities_SV.lvlib"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
		</Item>
		<Item Name="SV.lib" Type="Folder">
			<Item Name="CSPP-Template.lvlib" Type="Library" URL="../SV.lib/CSPP-Template.lvlib"/>
			<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
		</Item>
		<Item Name="User" Type="Folder"/>
		<Item Name="CSPP-Minimum.ini" Type="Document" URL="../CSPP-Minimum.ini"/>
		<Item Name="CSPP-Template.ini" Type="Document" URL="../CSPP-Template.ini"/>
		<Item Name="CSPP-Template_Content.vi" Type="VI" URL="../CSPP-Template_Content.vi"/>
		<Item Name="CSPP-Template_Main.vi" Type="VI" URL="../CSPP-Template_Main.vi"/>
		<Item Name="README.md" Type="Document" URL="../README.md"/>
		<Item Name="Release_Notes.md" Type="Document" URL="../Release_Notes.md"/>
		<Item Name="Rename.vi" Type="VI" URL="../Rename.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="1D Array to String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/1D Array to String__ogtk.vi"/>
				<Item Name="Array of VData to VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VArray__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array Size(s)__ogtk.vi"/>
				<Item Name="Array to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array to Array of VData__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (Bool)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (Bool)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CDB)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CSG)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CTL-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CTL-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CXT)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (DBL)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (EXT)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (GEN-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (GEN-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (GObj-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (GObj-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I8)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I16)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I32)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I64)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (LVObject)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (Path)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (SGL)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (String)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U8)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U16)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U32)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U64)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (Variant)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (VI-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (VI-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from Array__ogtk.vi"/>
				<Item Name="Empty 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Empty 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Empty 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Empty 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Empty 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Empty 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Empty 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (I8)__ogtk.vi"/>
				<Item Name="Empty 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (I16)__ogtk.vi"/>
				<Item Name="Empty 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (I32)__ogtk.vi"/>
				<Item Name="Empty 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (I64)__ogtk.vi"/>
				<Item Name="Empty 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Empty 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (Path)__ogtk.vi"/>
				<Item Name="Empty 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Empty 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (String)__ogtk.vi"/>
				<Item Name="Empty 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (U8)__ogtk.vi"/>
				<Item Name="Empty 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (U16)__ogtk.vi"/>
				<Item Name="Empty 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (U32)__ogtk.vi"/>
				<Item Name="Empty 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (U64)__ogtk.vi"/>
				<Item Name="Empty 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Empty 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Empty 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Empty 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Empty 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Empty 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Empty 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Empty 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (I8)__ogtk.vi"/>
				<Item Name="Empty 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (I16)__ogtk.vi"/>
				<Item Name="Empty 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (I32)__ogtk.vi"/>
				<Item Name="Empty 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (I64)__ogtk.vi"/>
				<Item Name="Empty 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Empty 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (Path)__ogtk.vi"/>
				<Item Name="Empty 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Empty 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (String)__ogtk.vi"/>
				<Item Name="Empty 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (U8)__ogtk.vi"/>
				<Item Name="Empty 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (U16)__ogtk.vi"/>
				<Item Name="Empty 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (U32)__ogtk.vi"/>
				<Item Name="Empty 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (U64)__ogtk.vi"/>
				<Item Name="Empty 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Empty Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty Array (Variant)__ogtk.vi"/>
				<Item Name="Empty Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Empty Array__ogtk.vi"/>
				<Item Name="End of Line Constant (bug fix).vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/End of Line Constant (bug fix).vi"/>
				<Item Name="Filter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array__ogtk.vi"/>
				<Item Name="Get Array Element Default Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element Default Data__ogtk.vi"/>
				<Item Name="Get Array Element TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TD__ogtk.vi"/>
				<Item Name="Get Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TDEnum__ogtk.vi"/>
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Default Data from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Default Data from TD__ogtk.vi"/>
				<Item Name="Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Get Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder Array2__ogtk.vi"/>
				<Item Name="Reshape 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Reshape 1D Array__ogtk.vi"/>
				<Item Name="Reshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Reshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Search Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search Array__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Strip Units__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Strip Units__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Waveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Waveform Subtype Enum__ogtk.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="1D String Array to Delimited String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/1D String Array to Delimited String.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="cfis_Get File Extension Without Changing Case.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Get File Extension Without Changing Case.vi"/>
				<Item Name="cfis_Replace Percent Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Replace Percent Code.vi"/>
				<Item Name="cfis_Reverse Scan From String For Integer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Reverse Scan From String For Integer.vi"/>
				<Item Name="cfis_Split File Path Into Three Parts.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Split File Path Into Three Parts.vi"/>
				<Item Name="Check if Data Type has User-Definable String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Check if Data Type has User-Definable String__JKI EasyXML.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Cluster to Array of VData--EasyXML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Cluster to Array of VData--EasyXML__JKI EasyXML.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I32)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Conditional Auto-Indexing Tunnel (I32)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create File with Incrementing Suffix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Create File with Incrementing Suffix.vi"/>
				<Item Name="Date Type Format String Mapping__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Date Type Format String Mapping__JKI EasyXML.vi"/>
				<Item Name="Default MQTT Packet (Empty).vi" Type="VI" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/MQTT Control Packets/Control Packets/ControlPacket/Default MQTT Packet (Empty).vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Easy Generate XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Easy Generate XML__JKI EasyXML.vi"/>
				<Item Name="Easy Parse XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Easy Parse XML__JKI EasyXML.vi"/>
				<Item Name="EasyXML Options - Data Cluster__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/EasyXML Options - Data Cluster__JKI EasyXML.ctl"/>
				<Item Name="EasyXML Options - Type Formatting Cluster__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/EasyXML Options - Type Formatting Cluster__JKI EasyXML.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape-Unescape String for XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Escape-Unescape String for XML__JKI EasyXML.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Generic Data to XML Value__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Format Generic Data to XML Value__JKI EasyXML.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Format Time Stamp to XML dateTime string__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Format Time Stamp to XML dateTime string__JKI EasyXML.vi"/>
				<Item Name="Format Variant Into String--EasyXML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Format Variant Into String--EasyXML__JKI EasyXML.vi"/>
				<Item Name="Formatting Data Type -- Enum__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Formatting Data Type -- Enum__JKI EasyXML.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get Array Element Default Data--EasyXML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Array Element Default Data--EasyXML__JKI EasyXML.vi"/>
				<Item Name="Get Attributes__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Attributes__JKI EasyXML.vi"/>
				<Item Name="Get Children__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Children__JKI EasyXML.vi"/>
				<Item Name="Get Default Data from TD--EasyXML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Default Data from TD--EasyXML__JKI EasyXML.vi"/>
				<Item Name="Get Default Type Formatting__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Default Type Formatting__JKI EasyXML.vi"/>
				<Item Name="Get EasyXML Data Type__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get EasyXML Data Type__JKI EasyXML.vi"/>
				<Item Name="Get Element and Attributes from Tag__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Element and Attributes from Tag__JKI EasyXML.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get GObject Label.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/Get GObject Label.vi"/>
				<Item Name="Get Key-Value Pairs from Attribute List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Key-Value Pairs from Attribute List__JKI EasyXML.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get Next Name from Attribute List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Next Name from Attribute List__JKI EasyXML.vi"/>
				<Item Name="Get Next Value from Attribute List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Next Value from Attribute List__JKI EasyXML.vi"/>
				<Item Name="Get Node Attribute (String)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Attribute (String)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (Boolean)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (Boolean)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (DBL)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (DBL)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (EXT)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (EXT)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (I64)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (I64)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (String)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (String)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (U64)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (U64)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data__JKI EasyXML.vi"/>
				<Item Name="Get Root Elements__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Root Elements__JKI EasyXML.vi"/>
				<Item Name="Get Semaphore Status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Get Semaphore Status.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Tag Content__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Tag Content__JKI EasyXML.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get Type Code from I16 Array And Pos.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Get Type Code from I16 Array And Pos.vi"/>
				<Item Name="Get UTC Offset__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get UTC Offset__JKI EasyXML.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Index 1D Array Elements (I32)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Index 1D Array Elements (I32)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Index 1D Array Elements (String)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Index 1D Array Elements (String)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Is an Error (any error array element)__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error (any error array element)__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Is an Error (error array)__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error (error array)__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Is an Error (error cluster)__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error (error cluster)__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Is an Error__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="JKI JSON Serialization.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI.lib/Serialization/JSON/JKI JSON Serialization.lvlib"/>
				<Item Name="JKI Serialization.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI.lib/Serialization/Core/JKI Serialization.lvlib"/>
				<Item Name="JKI Unicode.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI.lib/Unicode/JKI Unicode.lvlib"/>
				<Item Name="KVP Parser.vi" Type="VI" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/Data Manipulation/KVP Parser.vi"/>
				<Item Name="Link Children__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Link Children__JKI EasyXML.vi"/>
				<Item Name="Link XML Start Tags with End Tags__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Link XML Start Tags with End Tags__JKI EasyXML.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Lookup Format String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Lookup Format String__JKI EasyXML.vi"/>
				<Item Name="LV70DateRecToTimeStamp.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/LV70DateRecToTimeStamp.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MD5Checksum core.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
				<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
				<Item Name="MD5Checksum pad.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
				<Item Name="MD5Checksum string.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum string.vi"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="MQTT Base.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/MQTT Connection/MQTT_Base/MQTT Base.lvlib"/>
				<Item Name="MQTT Client.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/MQTT Client/MQTT Client.lvlib"/>
				<Item Name="MQTT_Connection.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/MQTT Connection/MQTT_Connection/MQTT_Connection.lvlib"/>
				<Item Name="MQTT_Control_Packets.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/MQTT Control Packets/Control Packets/MQTT_Control_Packets.lvlib"/>
				<Item Name="MQTT_TCP.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/MQTT TCP Connection/MQTT_TCP/MQTT_TCP.lvlib"/>
				<Item Name="Multiline String to Array (Preserve EOLs)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Multiline String to Array (Preserve EOLs)__JKI EasyXML.vi"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="OpenConvert.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/Data Manipulation/Conversions/OpenConvert.lvlib"/>
				<Item Name="OpenDescriptor.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/Data Manipulation/TypeDescriptor/OpenDescriptor.lvlib"/>
				<Item Name="OpenScalar.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/Data Manipulation/Scalar/OpenScalar.lvlib"/>
				<Item Name="OpenSerializer.easyXML.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/OpenSerializer easyXML/OpenSerializer.easyXML.lvlib"/>
				<Item Name="OpenSerializer.JKI-JSON.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/OpenSerializer JKI JSON/OpenSerializer.JKI-JSON.lvlib"/>
				<Item Name="OpenSerializer.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/OpenSerializer/OpenSerializer.lvlib"/>
				<Item Name="OpenVariant.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/Data Manipulation/Variant/OpenVariant.lvlib"/>
				<Item Name="Parse XML dateTime String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Parse XML dateTime String__JKI EasyXML.vi"/>
				<Item Name="Parse XML for Tags__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Parse XML for Tags__JKI EasyXML.vi"/>
				<Item Name="Parse XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Parse XML__JKI EasyXML.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="Remove Comments from XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Comments from XML__JKI EasyXML.vi"/>
				<Item Name="Remove DOCTYPE from XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove DOCTYPE from XML__JKI EasyXML.vi"/>
				<Item Name="Remove Headers from XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Headers from XML__JKI EasyXML.vi"/>
				<Item Name="Remove Indentation__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Indentation__JKI EasyXML.vi"/>
				<Item Name="Remove Raw XML Tag from Data Name__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Raw XML Tag from Data Name__JKI EasyXML.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search 1D Array (String)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Search 1D Array (String)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Serializer.Base64.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/LabVIEW Open Source Project/OpenSerializer.Base64/Serializer.Base64.lvclass"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Set VISA IO Session String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Set VISA IO Session String__JKI EasyXML.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TD_Compare Types.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Compare Types.vi"/>
				<Item Name="TD_Create Array.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Create Array.vi"/>
				<Item Name="TD_Create Cluster.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Create Cluster.vi"/>
				<Item Name="TD_Get Array Information.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Get Array Information.vi"/>
				<Item Name="TD_Get Cluster Information.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Get Cluster Information.vi"/>
				<Item Name="TD_Get Ref Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Get Ref Info.vi"/>
				<Item Name="TD_Length.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Length.ctl"/>
				<Item Name="TD_Refnum Kind.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/TD_Refnum Kind.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Toolkit Error Handling - Add Caller to API VI List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Add Caller to API VI List__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - API VI List Buffer Core__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - API VI List Buffer Core__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - Error Cluster From Error Code__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Error Cluster From Error Code__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - Get API VI List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Get API VI List__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - Trim Call Chain at First API VI__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Trim Call Chain at First API VI__JKI EasyXML.vi"/>
				<Item Name="Treat Data Name__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Treat Data Name__JKI EasyXML.vi"/>
				<Item Name="Treat Entity Name__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Treat Entity Name__JKI EasyXML.vi"/>
				<Item Name="TRef Find Object By Label.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Find Object By Label.vi"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Type Code.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Type Code.ctl"/>
				<Item Name="Type Descriptor I16 Array.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Type Descriptor I16 Array.ctl"/>
				<Item Name="Type Descriptor I16.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Type Descriptor I16.ctl"/>
				<Item Name="Type Enum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Type Enum.ctl"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="Variant to XML - core - __JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Variant to XML - core - __JKI EasyXML.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
				<Item Name="XML Build Entity__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Build Entity__JKI EasyXML.vi"/>
				<Item Name="XML Get Node Data by Variant__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Get Node Data by Variant__JKI EasyXML.vi"/>
				<Item Name="XML Loop Stack Recursion__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Loop Stack Recursion__JKI EasyXML.vi"/>
				<Item Name="XML Structure - Cluster__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Structure - Cluster__JKI EasyXML.ctl"/>
				<Item Name="XML Tag Type - Enum__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Tag Type - Enum__JKI EasyXML.ctl"/>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CSPP-Template" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{773E39E1-BBD6-4173-9460-6FE7C7522091}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/CSPP-Template.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{95E968AE-FBA3-4AF4-B59D-842A4FF56C3A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP-Template</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-Template</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Packages/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6211658F-9962-4EFC-81EE-AF45E75227E4}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP-Template.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-Template/CSPP-Template.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-Template/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{8102B0CC-938C-42EA-A049-F1774B4A9155}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/CSPP-Template_Main.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Release_Notes.md</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/CSPP-Template.ini</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager.ini</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities.ini</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core.ini</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/SV.lib/CSPP-Template.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">9</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP-Template</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP-Template</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP-Template</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP-Template.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>

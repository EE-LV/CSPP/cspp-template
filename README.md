CSPP-Template README
====================
_CSPP-Template.lvproj_ is used to develop an application based on NI ActorFramework and CS++ libraries.
Currently used development SW is LabVIEW 2021 SP1. It now supports MQTT as alternativ to shared network valiables.

LONG_DESCRIPTION

Related documents and information
=================================
- README.md
- Release_Notes.md
- EUPL v.1.1 - Lizenz.pdf & EUPL v.1.1 - Lizenz.rtf
- Contact: your email
- Download, bug reports... : Git Repository URL
- Documentation:
  - Refer to Documantation folder 
  - NI Actor Framework: https://ni.com/actorframework
  - CS++
	- https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home
    - https://git.gsi.de/EE-LV/CSPP/CSPP_Documentation
  
Included Submodules
===================
- [Packages/CSPP_Core](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): This package is used as submodule.
- [Packages/CSPP_ObjectManager](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager): This package is used as submodule.
- [Packages/CSPP_DSC](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC): Containing DSC Alarm- & Trend-Viewer
- [Packages/CSPP_Utilities](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities): Providing some usefull utility classes. 
- [Packages/CSPP_MQTT](https://git.gsi.de/EE-LV/CSPP/CSPP_MQTT): Providing some classes supporting MQTT. 

Optional Submodules
-------------------
- [Packages/CSPP_DeviceBase](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): Definition of CS++Device ancestor classes
- [Packages/CSPP_IVI](https://git.gsi.de/EE-LV/CSPP/CSPP_IVI): Implementations of derived CS++Device classes using IVI driver
- [Packages/CSPP_LNA](https://git.gsi.de/EE-LV/CSPP/CSPP_LNA): Extends the Linked Network Actor to support zero coupled messages.
- [Packages/CSPP_RT](https://git.gsi.de/EE-LV/CSPP/CSPP_RT): Providing a librarie supporting LabVIEW-RT features. 
- [Packages/CSPP_PVConverter](https://git.gsi.de/EE-LV/CSPP/CSPP_PVConverter): Providing support for e.g. log-scaling of PV or conversion to array. 
- [Packages/CSPP_Syslog](https://git.gsi.de/EE-LV/CSPP/CSPP_Syslog): Providing a Syslog based Message Handler 
- [Packages/CSPP_Vacuum](https://git.gsi.de/EE-LV/CSPP/CSPP_Vacuum): Providing a DeviceActors dealing with vacuum. 

Refer to https://git.gsi.de/EE-LV/CSPP for more available CS++ submodules.

Optional External Dependencies
=================================
- CSPP_DSC and CSPP_MQTTSVInterface depend on LabVIEW Data Logging and Supervisory Control Module
- CSPP_MQTT depends on some VIPM Packages dealing with MQTT from LabVIEW Open Source Project. Refer to VIPM-MQTT.PNG for details.
  - You can find the sources at https://github.com/LabVIEW-Open-Source
  - Refer also to following youtube videos for an overview of this liberaies.
    - https://www.youtube.com/watch?v=Y-jrwyfD9DU&feature=youtu.be
    - https://youtu.be/Y-jrwyfD9DU

Optional External Dependencies
=================================
- Monitored Actor; Refer to
  - https://decibel.ni.com/content/thread/18301 and
  - http://lavag.org/topic/17056-monitoring-actors

Getting started:
=================================
- Install __CSPP_Tools__
  - Clone [CSPP_Tools](https://git.gsi.de/EE-LV/CSPP/CSPP_Tools)
  - Get submodules:
    - `git submodule init`
    - `git submodule update`
    - _Optionally switch to the most recent branch._
  - Mass-compile folder `CSPP_Tools`
  - Run `CSPP_Tools\Main-Project\Installer.vi`
- Fork __this__ repository, if not alread done, and rename repository name and path. Refer to repository settings.
- Clone the forked repository to a local folder.
- Switch to the desired branch.
- Get submodules:
  - `git submodule init`
  - `git submodule update`
  - _Optionally switch to the most recent branch._
- Optionally create a hard link to the custom error file(s): (admin permission maybe necessary; use cmd on windows not bash)
  - cd <LabVIEW>\user.lib\errors
  - mklink /h CSPP_Core-errors.txt Packages\CSPP_Core\CSPP_Core-errors.txt
- Rename `CSPP-Template.lvproj` to `YourProject.lvproj`
- Open `YourProject.lvproj` with LabVIEW
- If Data Logging and Supervisory Control Module is available
  - Add DSC stuff to project from `CSPP_DSC` and `CSPP_MQTT`
    - Add classes to CSPP-Template_Content.vi
    - Add corresponding entries in ini file.
	- Active DSC stuff in CSPP_DeploymentExample.vi
  - Save and reopen `YourProject.lvproj`
- Run `Rename.vi`
  - When all renaming was succefull, remove `Rename.vi` from project and delete from disc.
- Deploy Shared Variable libraries if used.
	- SV.lib/`YourProject.lvlib`
	- SV.lib/`CSPP_Core_SV.lvlib` If you want to use BaseActor and or DeviceActor.
- Save `YourProject.lvproj`.
- Run your project specific `YourProject_Main.vi` in order to check if everything is working.
- `chmod a-w -R *` to avoid unintended changes.

Start Implementing YourProject
===================================
- Extend `YourProject.lvproj` to your needs.
  - Edit copyright information in description of `YourProject.lvproj` and README.md
  - Use existing libraries and actors
    - Extend the `YourProject.ini` with additiional entries.
	  - Replace localhost with the adress and port of your MQTT broker in CSPP_MQTTBroker
	    - CSPP_MQTTClient.Broker="localhost"
		- CSPP_MQTTBroker:CSPP_MQTTClient.Port=1883
	- You could also start your own ini-file with `CSPP_Minimum.ini`. You need to rename it to `YourProject.ini` or to specify it in you main.vi or as command-line paramater.
  - Add more submodules
    - Template configuration ini-files and Shared Variable libraries should be included in all submodules.
    - Add more actors to configuration ini-file by copying from templates and renaming.
    - You need to create and deploy your project specific shared variable libraries.
         - e.g. copy Shared Variables from `CSPP_Core_SV.lvlib` to `YourProject.lvlib` and rename.
  - Develop your project specific actor classes.
    - Provide template configuration ini-files and Shared Variable libraries

Known issues:
=============
- CSPP-Template executable crashes with Enhanced DSC support enabled in LV2021 SP1.
- DSC stuff removed from project or disabled.
- Refer also to issue tracker at https://github.com/LabVIEW-Open-Source
  - Broker: https://github.com/LabVIEW-Open-Source/LV-MQTT-Broker/issues
  - Client: https://github.com/LabVIEW-Open-Source/MQTT-Client/issues
  - Messages with Qos>0 are handled synchronously and can cause performance issues.
    - https://github.com/LabVIEW-Open-Source/LV-MQTT-Broker/issues/165

Author: name@domain

Copyright 2022  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.